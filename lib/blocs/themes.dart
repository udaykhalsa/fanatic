import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';

AppTheme darkTheme(){
  return AppTheme(
    id: 'dark_theme',
    description: '',
    data: ThemeData(
      appBarTheme: AppBarTheme(
        actionsIconTheme: IconThemeData(
          color: Colors.white10,
        ),
        color: Colors.black,
        textTheme: TextTheme(
          title: TextStyle(
            color: Colors.white,
          )
        )
      )
    )
  );
}

AppTheme lightTheme(){
  return AppTheme(
    id: 'light_theme',
    description: '',
    data: ThemeData(
      appBarTheme: AppBarTheme(
        actionsIconTheme: IconThemeData(
          color: Colors.black,
        ),
        color: Colors.white,
        textTheme: TextTheme(
          title: TextStyle(
            color: Colors.black,
          )
        )
      )
    )
  );
}