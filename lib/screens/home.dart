import 'package:fanatic_messenger/screens/settings.dart';
import 'package:flutter/material.dart';
import 'chatscreen.dart';
import 'package:fanatic_messenger/constants.dart';

const nameHeadingHome= TextStyle(
  fontSize: 20,
);


const messagePreviewHome= TextStyle(
  fontSize: 14,
);


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
 
class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Messages'),
          actions: <Widget>[
            PopupMenuButton <String>(
                onSelected: choiceAction,
                itemBuilder: (BuildContext context){
                  return PopUpConstants.choices.map((String choices){
                    return PopupMenuItem<String>(
                        value: choices,
                        child: Text(choices),
                      );
                  }).toList();
                },
              )
          ],
        ),
      body: Column(
        children:[
          GestureDetector(
            onTap: () => Navigator.push(context, 
              MaterialPageRoute(builder: (context) => ChatScreen())),
              child: Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    GestureDetector(
                      // onTap: () => ,
                      child: Container(
                        height: 45,
                        width: 45,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/imgs/chat_icon.png'),
                            fit: BoxFit.fill,
                            alignment: Alignment.center
                         ),
                          // color: Colors.pink,
                          borderRadius: BorderRadius.all(
                            Radius.circular(50)
                          )
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Name',
                            style: nameHeadingHome,
                          ),
                          Text('This is a message from name',
                            style: messagePreviewHome,
                            // overflow: TextOverflow.fade
                          )
                        ],
                      ),
                    )               
                  ],
                ),
              ),
          )
        ],
      ),
    );      
  
  }

  void choiceAction(String choice){
    if (choice == 'Settings'){
      Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));
    }
  }

}


