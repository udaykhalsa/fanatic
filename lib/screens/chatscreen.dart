import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Name'),
        actions: [
          CircleAvatar(
            backgroundImage: NetworkImage('https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'),
          ),
        ],
      ),
      body: Column(
          children: [
            Expanded(
              child: Container(
              ),
            ),
            Positioned(
              bottom: 10,
              child: Row(
                children: [
                  Container(
                      child: IconButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () => {},
                      icon: Icon(Entypo.emoji_happy)
                    ),
                  ),
                  Expanded(
                      child: Container(
                      // height: 20,
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Message'
                        ) ,
                      ),
                    ),
                  ),
                  Container(
                      child: IconButton(
                      onPressed: () => {},
                      icon: Icon(Entypo.plus)
                    ),
                  ),
                ],
              ),
            )
          ],
        )
      );
  }
}